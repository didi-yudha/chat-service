package repository

import (
	"log"

	"bitbucket.org/didiyudha/chat/model"
	"github.com/asaskevich/govalidator"
	"github.com/jmoiron/sqlx"
)

type (
	// MessageCreator - create message abstraction
	MessageCreator interface {
		Create(msg *model.Message) (uint64, error)
	}

	// AllFinder - find all message abstraction
	AllFinder interface {
		FindAll() ([]*model.Message, error)
	}

	// MessageFinder - find message abstraction
	MessageFinder interface {
		AllFinder
	}

	// Message repository
	Message interface {
		MessageCreator
		MessageFinder
	}
)

type (
	pgMessage struct {
		DB *sqlx.DB
	}
)

// NewMessage - instance new message repository
func NewMessage(db *sqlx.DB) Message {
	return &pgMessage{
		DB: db,
	}
}

func (pg *pgMessage) Create(msg *model.Message) (uint64, error) {
	log.Println("Here")
	var ID uint64
	if _, err := govalidator.ValidateStruct(msg); err != nil {
		return ID, err
	}
	q := `INSERT INTO messages (content, created_at, updated_at) VALUES ($1, $2, $3) returning id`
	stmt, err := pg.DB.Prepare(q)
	if err != nil {
		return ID, err
	}
	rows, err := stmt.Query(msg.Content, msg.CreatedAt, msg.UpdatedAt)
	if err != nil {
		return ID, err
	}
	defer rows.Close()
	for rows.Next() {
		if err := rows.Scan(&ID); err != nil {
			return ID, err
		}
	}
	return ID, nil
}

func (pg *pgMessage) FindAll() ([]*model.Message, error) {
	messages := make([]*model.Message, 0)
	q := `SELECT id, 
			content, 
			created_at,
			updated_at,
			deleted_at
		FROM messages
		WHERE deleted_at isnull`
	stmt, err := pg.DB.Prepare(q)
	if err != nil {
		return messages, err
	}
	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var msg model.Message
		if err := rows.Scan(&msg.ID,
			&msg.Content,
			&msg.CreatedAt,
			&msg.UpdatedAt,
			&msg.DeleteddAt,
		); err != nil {
			return messages, err
		}
		messages = append(messages, &msg)
	}
	if rows.Err() != nil {
		return messages, rows.Err()
	}
	return messages, nil
}

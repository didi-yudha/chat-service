package repository

import (
	"errors"
	"testing"
	"time"

	"bitbucket.org/didiyudha/chat/model"
	"github.com/icrowley/fake"
	"github.com/jmoiron/sqlx"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestCreateMessage(t *testing.T) {
	tabletests := []struct {
		Description   string
		Data          model.Message
		ExpectedID    uint64
		ExpectedError error
	}{
		{
			Description: "Should save message successfully",
			Data: model.Message{
				Content:   fake.MaleFirstName(),
				CreatedAt: time.Now(),
				UpdatedAt: time.Now(),
			},
			ExpectedID:    1,
			ExpectedError: nil,
		},
		{
			Description: "Should failed save message because content is empty",
			Data: model.Message{
				CreatedAt: time.Now(),
				UpdatedAt: time.Now(),
			},
			ExpectedID:    0,
			ExpectedError: errors.New("Content message can not be empty"),
		},
	}
	dbMock, mock, err := sqlmock.New()
	if err != nil {
		t.Error(err)
	}
	defer dbMock.Close()
	db := sqlx.NewDb(dbMock, "sqlmock")
	msgrepo := NewMessage(db)
	rows := sqlmock.NewRows([]string{"id"}).AddRow(1)
	for _, datatest := range tabletests {
		mock.ExpectPrepare(`INSERT INTO messages`)
		mock.ExpectQuery(`INSERT INTO messages`).
			WithArgs(datatest.Data.Content, datatest.Data.CreatedAt, datatest.Data.UpdatedAt).
			WillReturnRows(rows)
		t.Log(datatest.Description)
		{
			id, err := msgrepo.Create(&datatest.Data)
			if id != datatest.ExpectedID {
				t.Errorf("\tExpected ID %d but actual %d\n", datatest.ExpectedID, id)
			}
			if (err != nil && datatest.ExpectedError == nil) || (err == nil && datatest.ExpectedError != nil) {
				t.Errorf("\tExpected ID %v but actual %v\n", datatest.ExpectedError, err)
			}
			if (err != nil && datatest.ExpectedError != nil) && (err.Error() != datatest.ExpectedError.Error()) {
				t.Errorf("\tExpected error message %s but actual %s\n", datatest.ExpectedError.Error(), err.Error())
			}
		}
	}
}

func TestFindAllMessages(t *testing.T) {
	dbMock, mock, err := sqlmock.New()
	if err != nil {
		t.Error(err)
	}
	defer dbMock.Close()
	db := sqlx.NewDb(dbMock, "sqlmock")
	msgrepo := NewMessage(db)
	rows := sqlmock.NewRows([]string{"id", "content",
		"created_at",
		"updated_at",
		"deleted_at"}).
		AddRow(1, fake.FullName(), time.Now(), time.Now(), nil)
	mock.ExpectPrepare(`SELECT id`)
	mock.ExpectQuery(`SELECT id`).WillReturnRows(rows)

	t.Log("Should find all messages successfully")
	{
		messages, err := msgrepo.FindAll()
		if err != nil {
			t.Errorf("\tExpected error message %v but actual %v\n", nil, err)
		}
		if len(messages) != 1 {
			t.Errorf("\tExpected len message %d but actual %d\n", 1, len(messages))
		}
	}
}

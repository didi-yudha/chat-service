# chat-service

### Database
Create a database **chat** in **PostgreSQL** database

### Build
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build bitbucket.org/didiyudha/chat

### Run
go run main.go

### Create Message
POST http://localhost:8080/message
{
    "message": "Hi"
}
### Response
{
    "status": 201,
    "code": 201,
    "message": "success",
    "data": {
        "message": {
            "id": 1,
            "content": "Hi",
            "createdAt": "2001-01-01 01:01:01",
            "updatedAt": "2001-01-01 01:01:01",
            "createdAt": null,
        }
    }
}

### Create Message
GET http://localhost:8080/message
### Response
{
    "status": 201,
    "code": 201,
    "message": "ok",
    "data": {
        "messages":[
             {
                "id": 3,
                "content": "Hello",
                "createdAt": "2018-03-06T09:33:58.961541Z",
                "updatedAt": "2018-03-06T09:33:58.961541Z",
                "deletedAt": null
            },
            {
                "id": 4,
                "content": "Hi",
                "createdAt": "2018-03-06T09:34:14.039349Z",
                "updatedAt": "2018-03-06T09:34:14.03935Z",
                "deletedAt": null
            }
        ]
    }
}

### Broadcast Message
POST http://localhost:8080/broadcasts
{
    "message": "Hi"
}
### Response
{
    "status": 201,
    "code": 201,
    "message": "success",
    "data": {
        "message": {
            "id": 1,
            "content": "Hi",
            "createdAt": "2001-01-01 01:01:01",
            "updatedAt": "2001-01-01 01:01:01",
            "createdAt": null,
        }
    }
}
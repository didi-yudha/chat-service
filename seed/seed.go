package seed

import "github.com/jmoiron/sqlx"

// Seed - seed DDL and DML
type Seed struct {
	DB *sqlx.DB
}

var (
	qCreateMessageTable = `CREATE TABLE IF NOT EXISTS messages (
		id SERIAL PRIMARY KEY,
		content VARCHAR NOT NULL,
		created_at TIMESTAMP NOT NULL,
		updated_at TIMESTAMP NOT NULL,
		deleted_at TIMESTAMP
	);`
)

// CreateDrop - create or drop messages table
func (s *Seed) CreateDrop() {
	s.DB.MustExec(qCreateMessageTable)
}

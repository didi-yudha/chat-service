package response

import (
	"encoding/json"
	"net/http"
)

// R - Response data to the client
type R struct {
	Status  int                    `json:"status"`
	Code    int                    `json:"code"`
	Message string                 `json:"message"`
	Data    map[string]interface{} `json:"data"`
}

// BadRequest - Bad request response (400)
func BadRequest(w http.ResponseWriter, msg string) {
	data := R{
		Status:  http.StatusBadRequest,
		Code:    http.StatusBadRequest,
		Message: "Bad Request",
		Data:    map[string]interface{}{"message": msg},
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(400)
	json.NewEncoder(w).Encode(&data)
}

// Created - Created response (201)
func Created(w http.ResponseWriter, entityName string, entity interface{}) {
	data := R{
		Status:  http.StatusCreated,
		Code:    http.StatusCreated,
		Message: "Created",
		Data:    map[string]interface{}{entityName: entity},
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(data)
}

// InternalServerError - Internal server error
func InternalServerError(w http.ResponseWriter, msg string) {
	data := R{
		Status:  http.StatusInternalServerError,
		Code:    http.StatusInternalServerError,
		Message: "Internal Server Error",
		Data:    map[string]interface{}{"message": msg},
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(500)
	json.NewEncoder(w).Encode(&data)
}

// OK - OK response
func OK(w http.ResponseWriter, entityName string, entities interface{}) {
	data := R{
		Status:  http.StatusOK,
		Code:    http.StatusOK,
		Message: "OK",
		Data:    map[string]interface{}{entityName: entities},
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(&data)
}

// NotFound - Not found response
func NotFound(w http.ResponseWriter, msg string) {
	data := R{
		Status:  http.StatusNotFound,
		Code:    http.StatusNotFound,
		Message: "Created",
		Data:    map[string]interface{}{"message": msg},
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(404)
	json.NewEncoder(w).Encode(&data)
}

package db

import (
	"errors"
	"log"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

const (
	maxAttempt = 10
)

// ErrUnableToConnectToDB - Error message when system unable to connect to database
var ErrUnableToConnectToDB = errors.New("Unable to connect to database")

// NewDB - Instantiate and check connection to Postgre database
func NewDB(c Configurator) (*sqlx.DB, error) {
	db, err := sqlx.Connect(c.DBDriver(), c.DataSource())
	if err != nil {
		return nil, ErrUnableToConnectToDB
	}
	var dbError error
	for i := 1; i <= maxAttempt; i++ {
		dbError = db.Ping()
		if dbError == nil {
			break
		}
		log.Println("[ERROR]", dbError)
		time.Sleep(time.Duration(i) * time.Second)
	}
	if dbError != nil {
		log.Fatal(dbError)
	}
	return db, nil
}

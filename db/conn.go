package db

type (
	// Configurator - DB connection configurator
	Configurator interface {
		DBDriver() string
		DataSource() string
	}
)

package model

import "time"

type (

	// Message model
	Message struct {
		ID         uint64     `json:"id" db:"id" valid:"-"`
		Content    string     `json:"content" db:"content" valid:"required~Content message can not be empty"`
		CreatedAt  time.Time  `json:"createdAt" db:"created_at" valid:"required~CreatedAt can not empty"`
		UpdatedAt  time.Time  `json:"updatedAt" db:"updated_at" valid:"required~UpdatedAt can not empty"`
		DeleteddAt *time.Time `json:"deletedAt" db:"deleted_at" valid:"-"`
	}
)

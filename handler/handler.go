package handler

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/asaskevich/govalidator"

	"bitbucket.org/didiyudha/chat/fcm"
	"bitbucket.org/didiyudha/chat/model"
	"bitbucket.org/didiyudha/chat/response"
	"bitbucket.org/didiyudha/chat/service"
	"github.com/gorilla/mux"
)

type (
	// Message handler
	Message struct {
		Creator     service.MessageCreator
		Finder      service.MessageFinder
		Broadcaster fcm.BroadCaster
	}
)

// NewMessage - instance new message handler
func NewMessage(s service.Message, m fcm.Messanger) *Message {
	return &Message{
		Creator:     s,
		Finder:      s,
		Broadcaster: m,
	}
}

// CreateMessageRouter - create message router
func CreateMessageRouter(router *mux.Router, msg *Message) {
	router.
		Methods(http.MethodPost).
		Path("/messages").
		HandlerFunc(msg.Create)
	router.
		Methods(http.MethodGet).
		Path("/messages").
		HandlerFunc(msg.FindAll)
	router.
		Methods(http.MethodPost).
		Path("/broadcasts").
		HandlerFunc(msg.Broadcast)
}

// Create message handler
func (m *Message) Create(w http.ResponseWriter, r *http.Request) {
	body := struct {
		Message string `json:"message" valid:"required~Message can not be empty"`
	}{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		response.InternalServerError(w, err.Error())
		return
	}
	if _, err := govalidator.ValidateStruct(&body); err != nil {
		response.BadRequest(w, err.Error())
		return
	}
	message := model.Message{
		Content:   body.Message,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	if err := m.Creator.CreateMessage(&message); err != nil {
		response.InternalServerError(w, err.Error())
		return
	}
	response.Created(w, "message", message)
}

// FindAll - find all messages handler
func (m *Message) FindAll(w http.ResponseWriter, r *http.Request) {
	messages, err := m.Finder.FindAll()
	if err != nil {
		response.InternalServerError(w, err.Error())
		return
	}
	response.OK(w, "messages", messages)
}

// Broadcast handler
func (m *Message) Broadcast(w http.ResponseWriter, r *http.Request) {
	body := struct {
		Message string `json:"message" valid:"required~Message can not be empty"`
	}{}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		response.InternalServerError(w, err.Error())
		return
	}
	if _, err := govalidator.ValidateStruct(&body); err != nil {
		response.BadRequest(w, err.Error())
		return
	}
	if err := m.Broadcaster.BroadCast("/topics/broadcasts", body); err != nil {
		response.InternalServerError(w, err.Error())
		return
	}
	msg := model.Message{
		Content:   body.Message,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	if err := m.Creator.CreateMessage(&msg); err != nil {
		response.InternalServerError(w, err.Error())
		return
	}
	response.Created(w, "message", msg)
}

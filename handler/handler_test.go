package handler

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"bitbucket.org/didiyudha/chat/model"
	"github.com/asaskevich/govalidator"
)

type (
	// MessageServiceMock - mocking message service
	MessageServiceMock struct{}
	MessangerMock      struct{}
)

func (ms *MessageServiceMock) CreateMessage(msg *model.Message) error {
	if _, err := govalidator.ValidateStruct(msg); err != nil {
		return err
	}
	msg.ID = 1
	return nil
}

func (ms *MessageServiceMock) FindAll() ([]*model.Message, error) {
	messages := []*model.Message{
		&model.Message{
			ID:         1,
			Content:    "Hello",
			CreatedAt:  time.Now(),
			UpdatedAt:  time.Now(),
			DeleteddAt: nil,
		},
		&model.Message{
			ID:         2,
			Content:    "World",
			CreatedAt:  time.Now(),
			UpdatedAt:  time.Now(),
			DeleteddAt: nil,
		},
		&model.Message{
			ID:         3,
			Content:    "Hi",
			CreatedAt:  time.Now(),
			UpdatedAt:  time.Now(),
			DeleteddAt: nil,
		},
	}
	return messages, nil
}

func (m *MessangerMock) BroadCast(topic string, data interface{}) error {
	if len(topic) == 0 {
		return errors.New("Topic is empty")
	}
	return nil
}

func TestCreateMessageHandler(t *testing.T) {
	tabletests := []struct {
		Description  string
		Data         interface{}
		ExpectedCode int
	}{
		{
			Description: "Should successfully posted a message",
			Data: struct {
				Message string `json:"message"`
			}{
				Message: "Hello",
			},
			ExpectedCode: 201,
		},
		{
			Description: "Should successfully posted a message",
			Data: struct {
				Message string `json:"message"`
			}{
				Message: "",
			},
			ExpectedCode: 400,
		},
	}
	serviceMock := new(MessageServiceMock)
	for _, datatest := range tabletests {
		rw := httptest.NewRecorder()
		bodyBytes, err := json.Marshal(&datatest.Data)
		if err != nil {
			t.Error(err)
		}
		msgHandler := &Message{
			Creator: serviceMock,
		}
		req, err := http.NewRequest("POST", "/messages", bytes.NewBuffer(bodyBytes))
		if err != nil {
			t.Error(err)
		}
		handler := http.HandlerFunc(msgHandler.Create)
		handler.ServeHTTP(rw, req)
		if rw.Code != datatest.ExpectedCode {
			t.Errorf("Expected status code is %d but actual is %d", datatest.ExpectedCode, rw.Code)
		}
	}
}

func TestFindAllMessageHandler(t *testing.T) {
	rw := httptest.NewRecorder()
	serviceMock := new(MessageServiceMock)
	msgHandler := &Message{
		Finder: serviceMock,
	}
	req, err := http.NewRequest("GET", "/messages", nil)
	if err != nil {
		t.Error(err)
	}
	handler := http.HandlerFunc(msgHandler.FindAll)
	handler.ServeHTTP(rw, req)
	if rw.Code != 200 {
		t.Errorf("Expected status code is %d but actual is %d", 200, rw.Code)
	}
}

func TestBroadcastMessage(t *testing.T) {
	body := struct {
		Message string `json:"message"`
	}{Message: "Hi"}

	serviceMock := new(MessageServiceMock)
	messangerMock := new(MessangerMock)

	rw := httptest.NewRecorder()
	bodyBytes, err := json.Marshal(&body)
	if err != nil {
		t.Error(err)
	}
	msgHandler := Message{
		Creator:     serviceMock,
		Broadcaster: messangerMock,
	}
	req, err := http.NewRequest("POST", "/messages", bytes.NewBuffer(bodyBytes))
	if err != nil {
		t.Error(err)
	}
	handler := http.HandlerFunc(msgHandler.Broadcast)
	handler.ServeHTTP(rw, req)
	if rw.Code != http.StatusCreated {
		t.Errorf("\tExpected status code %d but actual %d\n", http.StatusCreated, rw.Code)
	}
}

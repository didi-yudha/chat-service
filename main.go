package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"bitbucket.org/didiyudha/chat/config"
	"bitbucket.org/didiyudha/chat/db"
	"bitbucket.org/didiyudha/chat/fcm"
	"bitbucket.org/didiyudha/chat/handler"
	"bitbucket.org/didiyudha/chat/repository"
	"bitbucket.org/didiyudha/chat/seed"
	"bitbucket.org/didiyudha/chat/service"
	"github.com/asaskevich/govalidator"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	graceful "gopkg.in/tylerb/graceful.v1"
)

func init() {
	govalidator.SetFieldsRequiredByDefault(true)
	viper.AutomaticEnv()
	viper.SetEnvPrefix("CHAT")
}

func main() {
	router := mux.NewRouter()
	configFileName := viper.GetString("FILE_CONFIG")
	if len(configFileName) == 0 {
		configFileName = "config.yaml"
	}
	conf := config.Read(configFileName)
	db, err := db.NewDB(conf)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Seed
	seed := seed.Seed{
		DB: db,
	}
	seed.CreateDrop()

	// FCM
	client, err := fcm.NewClient(conf.FCMKey())
	if err != nil {
		log.Fatal(err)
	}
	messager := fcm.NewFirebaseCloudMessaging(client)

	// Message
	messageRepo := repository.NewMessage(db)
	messageService := service.NewMessage(messageRepo)
	messageHandler := handler.NewMessage(messageService, messager)
	handler.CreateMessageRouter(router, messageHandler)

	srv := &graceful.Server{
		Timeout: 15 * time.Second,
		Server: &http.Server{
			Addr:    conf.BindingPort(),
			Handler: router,
		},
	}
	fmt.Printf("chat service up on port %s\n", conf.BindingPort())
	log.Fatalln(srv.ListenAndServe())
}

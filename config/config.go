package config

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"
)

type (
	// Config - all information configuration
	Config struct {
		Port         string `yaml:"port"`
		FCMServerKey string `yaml:"fcmServerKey"`
		Psql         `yaml:"psql"`
	}

	// Psql - postgre config information
	Psql struct {
		Db       string `yaml:"db"`
		Name     string `yaml:"name"`
		Host     string `yaml:"host"`
		DBPort   string `yaml:"port"`
		Username string `yaml:"username"`
		Password string `yaml:"password"`
		SslMode  string `yaml:"ssl_mode"`
	}
)

// Read - read configuration file / env variable
func Read(fileName string) *Config {
	fileBytes, err := ioutil.ReadFile(fileName)
	config := &Config{}
	switch {
	case os.IsNotExist(err):
		ReadEnvConfig(config)
		break
	case err == nil:
		if err := yaml.Unmarshal(fileBytes, config); err != nil {
			ReadEnvConfig(config)
		}
		break
	}
	return config
}

// ReadEnvConfig - read environment variable
func ReadEnvConfig(config *Config) {
	if len(strings.TrimSpace(viper.GetString("DB_NAME"))) > 0 {
		config.Psql.Db = viper.GetString("DB_NAME")
	}
	if len(strings.TrimSpace(viper.GetString("DB_DRIVER"))) > 0 {
		config.Psql.Name = viper.GetString("DB_DRIVER")
	}
	if len(strings.TrimSpace(viper.GetString("DB_HOST"))) > 0 {
		config.Psql.Host = viper.GetString("DB_HOST")
	}
	if len(strings.TrimSpace(viper.GetString("DB_PORT"))) > 0 {
		config.Psql.DBPort = viper.GetString("DB_PORT")
	}
	if len(strings.TrimSpace(viper.GetString("DB_USERNAME"))) > 0 {
		config.Psql.Username = viper.GetString("DB_USERNAME")
	}
	if len(strings.TrimSpace(viper.GetString("DB_PASSWORD"))) > 0 {
		config.Psql.Password = viper.GetString("DB_PASSWORD")
	}
	if len(strings.TrimSpace(viper.GetString("DB_SSL_MODE"))) > 0 {
		config.Psql.SslMode = viper.GetString("DB_SSL_MODE")
	}
	if len(strings.TrimSpace(viper.GetString("PORT"))) > 0 {
		config.Port = viper.GetString("PORT")
	}
	log.Println("FCM SERVER KEY: ", viper.GetString("FCM_SERVER_KEY"))
	if len(strings.TrimSpace(viper.GetString("FCM_SERVER_KEY"))) > 0 {
		config.FCMServerKey = viper.GetString("FCM_SERVER_KEY")
	}
	log.Printf("%+v", config)
}

// DBDriver - return driver database name in used
func (c *Config) DBDriver() string {
	return c.Psql.Name
}

// DataSource - return datasource name database in used
func (c Config) DataSource() string {
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		c.Psql.Host, c.Psql.DBPort, c.Psql.Username, c.Psql.Password, c.Psql.Db)
	return psqlInfo
}

// BindingPort - get binding port
func (c *Config) BindingPort() string {
	return fmt.Sprintf(":%s", c.Port)
}

// FCMKey - get fcm server key
func (c *Config) FCMKey() string {
	return c.FCMServerKey
}

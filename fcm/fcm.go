package fcm

import (
	"errors"
	"strings"

	gofcm "github.com/NaySoftware/go-fcm"
)

var (
	// ErrTopicEmpty - error message when topic is empty
	ErrTopicEmpty = errors.New("Topic is empty")
	// ErrDataEmpty - error message when data that will be sent is empty
	ErrDataEmpty = errors.New("Data is empty")
	// ErrBroadcast - error message when unable to broadcast message
	ErrBroadcast = errors.New("Unable to broadcast message")
	// ErrServerKeyEmpty - error message when server key is empty
	ErrServerKeyEmpty = errors.New("Server key is empty")
)

type (
	// BroadCaster - broadcast message abstraction
	BroadCaster interface {
		BroadCast(topic string, data interface{}) error
	}

	// Messanger - send message abstraction
	Messanger interface {
		BroadCaster
	}
)
type (
	// Client - FCM Client
	Client struct {
		FCMClient *gofcm.FcmClient
	}

	// FirebaseCloudMessaging - Firebase cloud messaging
	FirebaseCloudMessaging struct {
		*Client
	}
)

// NewClient - instance new client FCM
func NewClient(serverKey string) (*Client, error) {
	var client = &Client{}
	if len(strings.TrimSpace(serverKey)) == 0 {
		return client, ErrServerKeyEmpty
	}
	client.FCMClient = gofcm.NewFcmClient(serverKey)
	return client, nil
}

// NewFirebaseCloudMessaging - Instance new FCM
func NewFirebaseCloudMessaging(client *Client) Messanger {
	return &FirebaseCloudMessaging{
		Client: client,
	}
}

// BroadCast - broadcast message to
func (f *FirebaseCloudMessaging) BroadCast(topic string, data interface{}) error {
	if len(strings.TrimSpace(topic)) == 0 {
		return ErrTopicEmpty
	}
	if data == nil {
		return ErrDataEmpty
	}
	f.FCMClient.NewFcmMsgTo(topic, data)
	status, err := f.FCMClient.Send()
	if err != nil {
		return ErrBroadcast
	}
	status.PrintResults()
	return nil
}

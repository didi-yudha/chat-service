package service

import (
	"errors"
	"log"

	"github.com/asaskevich/govalidator"

	"bitbucket.org/didiyudha/chat/model"
	"bitbucket.org/didiyudha/chat/repository"
)

// ErrEmptyReceivers - error message when receiver(s) are empty
var ErrEmptyReceivers = errors.New("Receivers can not be empty")

// ErrEmptyMessage - error message when message content is empty
var ErrEmptyMessage = errors.New("Message content can not be empty")

type (
	// MessageCreator - create message abstraction
	MessageCreator interface {
		CreateMessage(message *model.Message) error
	}

	// MessageAllFinder - find all service
	MessageAllFinder interface {
		FindAll() ([]*model.Message, error)
	}

	// MessageFinder - find message service
	MessageFinder interface {
		MessageAllFinder
	}

	// Message repository
	Message interface {
		MessageCreator
		MessageFinder
	}
)

type (
	messageImpl struct {
		MessageCreator   repository.MessageCreator
		MessageAllFinder repository.MessageFinder
	}
)

// NewMessage - instance new message service
func NewMessage(msgRepo repository.Message) Message {
	return &messageImpl{
		MessageCreator:   msgRepo,
		MessageAllFinder: msgRepo,
	}
}

// SendMessage - send message service
func (m *messageImpl) CreateMessage(message *model.Message) error {
	if _, err := govalidator.ValidateStruct(message); err != nil {
		return err
	}
	log.Println("Create message")
	id, err := m.MessageCreator.Create(message)
	if err != nil {
		return err
	}
	message.ID = id
	return nil
}

// FindAll - find all messages
func (m *messageImpl) FindAll() ([]*model.Message, error) {
	return m.MessageAllFinder.FindAll()
}

package service

import (
	"errors"
	"testing"
	"time"

	"bitbucket.org/didiyudha/chat/model"
	"github.com/asaskevich/govalidator"
)

type (
	messageRepoMock struct{}
)

func (m *messageRepoMock) Create(msg *model.Message) (uint64, error) {
	var ID uint64
	if _, err := govalidator.ValidateStruct(msg); err != nil {
		return ID, err
	}
	ID = 1
	msg.ID = ID
	return ID, nil
}

func (m *messageRepoMock) FindAll() ([]*model.Message, error) {
	msg := []*model.Message{
		&model.Message{
			ID:         1,
			Content:    "Hi",
			CreatedAt:  time.Now(),
			UpdatedAt:  time.Now(),
			DeleteddAt: nil,
		},
		&model.Message{
			ID:         2,
			Content:    "Hello",
			CreatedAt:  time.Now(),
			UpdatedAt:  time.Now(),
			DeleteddAt: nil,
		},
	}
	return msg, nil
}

func TestCreateMessage(t *testing.T) {
	tabletests := []struct {
		Description string
		Data        model.Message
		ExpectedID  uint64
		ExpectedErr error
	}{
		{
			Description: "Should save message succussfully",
			Data: model.Message{
				Content:   "Hello",
				CreatedAt: time.Now(),
				UpdatedAt: time.Now(),
			},
			ExpectedErr: nil,
			ExpectedID:  1,
		},
		{
			Description: "Should failed to save message because content is empty",
			Data: model.Message{
				CreatedAt: time.Now(),
				UpdatedAt: time.Now(),
			},
			ExpectedID:  0,
			ExpectedErr: errors.New("Content message can not be empty"),
		},
	}
	messageRepo := new(messageRepoMock)
	messageService := NewMessage(messageRepo)
	for _, datatest := range tabletests {
		t.Log(datatest.Description)
		{
			err := messageService.CreateMessage(&datatest.Data)
			if (err != nil && datatest.ExpectedErr == nil) || (err == nil && datatest.ExpectedErr != nil) {
				t.Errorf("\tExpected error %v but actual %v\n", datatest.ExpectedErr, err)
			}
			if err != nil && datatest.ExpectedErr != nil && err.Error() != datatest.ExpectedErr.Error() {
				t.Errorf("\tExpected err message %s but actual %s\n", datatest.ExpectedErr.Error(), err.Error())
			}
			if datatest.Data.ID != datatest.ExpectedID {
				t.Errorf("\tExpected id %d but actual %d\n", datatest.ExpectedID, datatest.Data.ID)
			}
		}
	}
}
